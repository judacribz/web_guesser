require 'sinatra'
if development?
	require 'sinatra/reloader'
end

@@reset = true
msg = ""
bg_color = "#FFF"
@@secret_num = 0
@@btn_val = ""
@@num_guesses = 0
@@visibility = ""

get '/' do
	erb :index, :locals => {:number => @@secret_num,
							:msg => msg,
							:bg_color => bg_color,
							:btn_val => @@btn_val,
							:visibility => @@visibility} 


	guess = params["guess"].to_i
	

	if @@reset
		msg = "Guess a number between 0 and 100!"
		@@secret_num = rand(100)+1
		@@reset = false
		bg_color = "#FFF"
		@@btn_val = "Guess!"
		@@visibility = "visible"
	else
		if (guess != nil && !@@reset)
			msg = check_guess(guess)
			bg_color = set_bg_color(guess)
		end
	end

	erb :index, :locals => {:number => @@secret_num,
							:msg => msg,
							:bg_color => bg_color,
							:btn_val => @@btn_val,
							:visibility => @@visibility}
end


def check_guess(guess)
	if (guess == @@secret_num)
		@@reset = true
		@@btn_val = "Reset?"
		@@visibility = "hidden"
		"#{guess} is correct!"
	else
		if (guess > @@secret_num)
			if (guess > @@secret_num+5)
				"Way too high!"
			else
				"Too high!"
			end
		elsif (guess < @@secret_num)
			if (guess < @@secret_num-5)
				"Way too low!"
			else
				"Too low!"
			end
		end
	end
end

def set_bg_color(guess)
	if (guess == @@secret_num)
		"#00cc00"
	elsif (guess > @@secret_num+5 || guess < @@secret_num-5)
		"#ff0000"
	else
		"#ff3333"
	end
end
